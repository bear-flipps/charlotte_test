This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### `How to run this project`
- You must have NPM or [https://yarnpkg.com/pt-BR/](Yarn) installed.
- After cloning the repository, just run npm install or yarn.

### `Tasks available`

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>