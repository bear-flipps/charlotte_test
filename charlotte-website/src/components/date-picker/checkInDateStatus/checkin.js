import React from 'react';
import styled from 'styled-components';

const CheckInWrapper = styled.div`
  flex-direction: column;
  display: flex;
  height: auto;
  width: 100%;
`;

const CheckInDate = styled.div`
  font-family: 'Heebo', sans-serif;
  font-size: 1.2rem;
  color: #555;
`;

const CheckInStatus = styled.div`
  font-family: 'Heebo', sans-serif;
  margin-bottom: 20px;
  font-weight: 300;
  font-size 1rem;
  color: #B5B5B5;
`;

export const CheckIn = ({inDate, outDate}) => {
  return (
    <CheckInWrapper>
      <CheckInDate >CHECK-IN</CheckInDate>
      <CheckInStatus>{inDate}</CheckInStatus>
      <CheckInDate>CHECK-OUT</CheckInDate>
      <CheckInStatus>{outDate}</CheckInStatus>
    </CheckInWrapper>
  )
};

export default CheckIn;