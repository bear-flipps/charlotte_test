import React from 'react';
import styled from 'styled-components';

const SearchButton = styled.button`
  font-family: 'Heebo', sans-serif;
  background-color: transparent;
  border: 2px solid #F98100;
  border-radius: 100px;
  position: relative;
  font-size: 0.8rem;
  font-weight: 600;
  overflow:hidden;
  cursor: pointer;
  margin: 30px 0;
  color: #F98100;
  width: 180px;
  height: 50px;

  &:after {
    position: absolute;
    transition: .3s;
    content: '';
    width: 0;
    bottom: 0;
    height: 3px;
    background: #F98100;
    height: 120%;
    left: -10%;
    transform: skewX(10deg);
  }

  &:hover {
    color: white;
  
    &:after {
      width: 110%;
      left: -5px;
    }
  }
`;

const Content = styled.span`
  position: relative;
  z-index: 10;
`;

export const Button = ({handleClick, buttonValue}) => {
  return(
    <SearchButton><Content>{buttonValue}</Content></SearchButton>
  )
}

export default Button;