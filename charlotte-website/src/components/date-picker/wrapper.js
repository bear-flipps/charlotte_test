import React, { Component } from 'react';
import styled from 'styled-components';
import DayPicker, { DateUtils } from 'react-day-picker';
import moment from 'moment';
import CheckIn from 'components/date-picker/checkInDateStatus/checkin';
import Button from 'components/date-picker/searchButton/button';

import 'react-day-picker/lib/style.css';

const DatePickerWrapper = styled.section`
  box-shadow: 0 -40px 46px 0 rgba(0,0,0,0.33);
  margin: -150px auto 50px;
  background-color: white;
  border-radius: 7px;
  max-width: 830px;
  padding: 20px 0;
  width: 100%;
`;

const DatePickerTitle = styled.h1`
  font-family: 'Montserrat', sans-serif;
  letter-spacing: 2.5px;
  text-align: center;
  font-size: 1.2rem;
  margin: 40px 0;

  @media (max-width:768px) {
    font-size: 1rem;
  }
`;

const DatePickerCheckIn = styled.div`
  margin: 0 auto;
  display: flex;
  width: 80%;

  @media (max-width:768px) {
    flex-direction: column-reverse;
  }
`;

const Days = styled.div`
  width: 50%;

  @media (max-width:768px) {
    width: 100%;
  }
`;

const Calendar = styled.div`
  align-self: flex-end;
  width: 50%;

  a {
    font-family: 'Heebo', sans-serif;
    float: right;
    color: #555;
  }

  @media (max-width:768px) {
    width: 100%;
  }
`;

class DatePicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstTimePicking: true,
      inDate: 'Choose a date',
      outDate: 'Choose a date',
      dayTracker: {from:null, to:null}
    }

    this.handleDayClick = this.handleDayClick.bind(this)
    this.updateDateRange = this.updateDateRange.bind(this)
  }

  handleDayClick = day => {
    const range = DateUtils.addDayToRange(day, this.state.dayTracker);
    const fixedInDate = moment(range.from).format('LL');
    const fixedOutDate = moment(range.to).format('LL');

    this.setState({
      inDate: fixedInDate,
      outDate: fixedOutDate,
      dayTracker: {
        from: range.from,
        to: range.to
      },
    });
  };
  
  handleResetClick = e => {
    e.preventDefault();
    this.setState({
      inDate: 'Choose a date',
      outDate: 'Choose a date',
      dayTracker: {
        from: null,
        to: null,
      }
    });
  };
  
  updateDateRange(e) {
    this.handleDayClick(e);
    this.props.setDateRange(this.state.dayTracker);
  }
  
  render() {
    const { from, to } = this.state.dayTracker;

    return(
      <DatePickerWrapper>
        <DatePickerTitle>Select the dates to stay in Charlotte</DatePickerTitle>
        <DatePickerCheckIn>
          <Days>
            <CheckIn
              inDate={this.state.inDate}
              outDate={this.state.outDate}
            />
            <Button
              buttonValue="Search Hotels"
            />
          </Days>
          <Calendar>
            <div className="RangeExample">
              <DayPicker
                numberOfMonths={1}
                selectedDays={[from, { from, to }]}
                onDayClick={this.updateDateRange}
                fixedWeeks
              />
              {from &&
                to &&
                <p>
                  {' '}<a href="." onClick={this.handleResetClick}>Reset Dates</a>
                </p>}
            </div>
          </Calendar>
        </DatePickerCheckIn>
      </DatePickerWrapper>
    )
  }
}

export default DatePicker;

