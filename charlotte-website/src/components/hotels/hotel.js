import React, { Component } from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import axios from 'axios';

// import Button from 'components/date-picker/searchButton/button';

const API_URL = "http://www.raphaelfabeni.com.br/rv/hotels.json";

//#region styles
const HotelsWrapper = styled.section`
  max-width: 1280px;
  margin: 0 auto;
  width: 100%;

  h1 {
    font-family: 'Montserrat', sans-serif;
    letter-spacing: 2.5px;
    text-align: center;
    font-size: 1.2rem;
    margin: 0 0 40px;
  }
`;

const HotelsInfoWrapper = styled.section`
  flex-direction: column;
  align-items: center;
  max-width: 1280px;
  margin: 0 auto;
  display: flex;
  width: 100%;
`;

const HotelsInfoItem = styled.div`
  box-shadow: 0 4px 17px 0 rgba(0,0,0,0.22);
  justify-content: space-around;
  align-items: center;
  background: #FFFFFF;
  border-radius: 5px;
  min-height: 280px;
  max-width: 840px;
  margin: 20px 0;
  display: flex;
  width: 100%;

  img {
    box-shadow: 0 4px 17px 0 rgba(0,0,0,0.22);
    display: inline-block;
    margin-left: -120px;
    border-radius: 4px;
    max-width: 238px;
    height: auto;
    width: 100%;
  }

  @media (max-width:768px) {
    flex-direction: column;
    padding: 15px 0;
    width: 90%;

    img {
      max-width: 195px;
      margin-left: 0;
    }
  }

  @media (max-width:320px) {
    width: 100%;
  }
`;

const HotelsInfoText = styled.div`
  border-right: 1px solid #D9D9D9;
  box-sizing: border-box;
  max-width: 400px;
  padding: 20px;
  width: 50%;

  h2 {
    font-family: 'Heboo', sans-serif;
    letter-spacing: 2;
    font-weight: 600;
    font-size: 1rem;
    color: #F98100;
    clear: left;
  }

  p {
    font-family: 'Montserrat', sans-serif;
    font-size: 0.8rem;
    color: #B5B5B5;
    clear: left;
  }

  img {
    margin-left: 0;
    box-shadow: none;
    height: 12px;
    width: 13px;
  }

  @media (max-width:768px) {
    border-bottom: 1px solid #D9D9D9;
    margin-bottom: 20px;
    border-right: none;
    width: 100%;
  }
  
`;

const Star = styled.div`
  margin-rigth: 10px;
  margin: 15px 5px 15px 0;
  float: left;
`;

const Button = styled.button`
  font-family: 'Heebo', sans-serif;
  background-color: transparent;
  border: 2px solid;
  border-color: ${props => props.history ? "#79BD1A" : "#F98100" };
  border-radius: 100px;
  position: relative;
  margin-right: 20px;
  font-size: 0.8rem;
  font-weight: 600;
  overflow:hidden;
  cursor: pointer;
  color: ${props => props.history ? "#79BD1A" : "#F98100" };
  width: 113px;
  height: 38px;

  span {
    position: relative;
    z-index: 10;
  }

  &:after {
    position: absolute;
    transition: .3s;
    content: '';
    width: 0;
    bottom: 0;
    height: 3px;
    background: ${props => props.history ? "#79BD1A" : "#F98100" };
    height: 120%;
    left: -10%;
    transform: skewX(10deg);
  }

  &:hover {
    color: white;

    &:after {
      width: 110%;
      left: -5px;
    }
  }
`;

const Price = styled.div`
  font-family: 'Montserrat';
  letter-spacing: 3px;
  font-size: 1.8rem;
  font-weight: 600;
  color: #79BD1A;
  
  span {
    font-weight: 400;
    font-size: 1rem;
  }
`;
//#endregion styles

class Hotels extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hotelsList: [],
    }
  }

  componentWillMount() {
    axios
      .get(API_URL)
      .then(response => {
        const hotelsArr = response.data.hotels;

        this.setState({
          hotelsList: hotelsArr,
        });
      })
      .catch(error => console.error(error));
  }

  render(){
    const {dateRange} = this.props;
    console.log('date', dateRange);

    return (
      <HotelsWrapper>
        <h1>
          Best choices from
          {dateRange.from instanceof Date ? dateRange.from.toDateString() : "?"}
          {` to `}
          {dateRange.to instanceof Date ? dateRange.to.toDateString() : "?"}
        </h1>
        <HotelsInfoWrapper>
          { this.state.hotelsList.map((hotel, index) => {
              return (
                <HotelsInfoItem key={index}>
                  <img src={hotel.image} alt=""/>
                  <HotelsInfoText>
                    { _.times(hotel.rate, (index) => {
                        return (
                          <Star key={index}>
                            <img src="images/star.svg" alt=""/>
                          </Star>
                        )
                      })
                    }
                    <h2>{hotel.name}</h2>
                    <p>{hotel.description}</p>
                    <Button><span>Book now</span></Button>
                    <Button history><span>Price History</span></Button>
                  </HotelsInfoText>
                  <Price>${hotel.price}<span>/day</span></Price>
                </HotelsInfoItem>
              )
          })}
        </HotelsInfoWrapper>
      </HotelsWrapper>
    )
  }
}

export default Hotels;
