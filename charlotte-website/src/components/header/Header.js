import React from 'react';
import styled from 'styled-components';

import Menu from 'components/header/menu/menu';

const Header = styled.header`
  background: url('images/hero.jpg');
  background-size: cover;
  height: 600px;
  width: 100%;
`;

const Logo = styled.div`
  flex-direction: column;
  align-items: center;
  max-width: 600px;
  margin: 20px auto;
  display: flex;
  width: 100%
`;

const Crown = styled.div`
  margin-bottom: 20px;
`;

const Greeting = styled.h2`
  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  letter-spacing: 3px;
  font-size: 1.2rem;
  font-weight: 600;
  color: white;
  margin: 0;
`;

const SperationLines = styled.div`
  background-color: white;
  margin: 10px 0;
  border-radius: 8px;
  width: 100%;
  height: 2px;

  @media (max-width: 768px) {
    width: 90%;
  }
`;

const Title = styled.h1`
  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  letter-spacing: 10px;
  font-size: 5rem;
  color: white;
  margin: 0;

  @media (max-width: 768px) {
    font-size: 10vw;
  }
`;

export const HeaderComponent = () => {
  return (
    <Header>
      <Menu></Menu>
      <Logo>
        <Crown>
          <img src="images/crown.svg" alt=""/>
        </Crown>
        <Greeting>WELCOME TO</Greeting>
        <SperationLines></SperationLines>
        <Title>CHARLOTTE</Title>
        <SperationLines></SperationLines>
        <Greeting>THE QUEEN CITY</Greeting>
      </Logo>
    </Header>
  )
}

export default HeaderComponent;