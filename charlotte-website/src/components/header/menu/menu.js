import React from 'react';
import styled from 'styled-components';

const Nav = styled.nav`
  position: relative;
  max-width: 1280px;
  padding: 36px 0;
  margin: 0 auto;
  width: 100%;
`;

const NavList = styled.ul`
  list-style: none;
  display: flex;
  margin: 0;

  @media (max-width:768px) {
    justify-content: space-around;
    padding: 0;
  }
`;

const NavItem = styled.li`
  margin-left: 40px;
  font-size: 1rem;

  @media (max-width:768px) {
    margin-left: 0;
  }

  a {
    text-decoration: none;
    color: white;
  }
`;

export const Menu = () => {
  return (
    <Nav>
      <NavList>
        <NavItem><a href="#">The Queen City</a></NavItem>
        <NavItem><a href="#">My Reservations</a></NavItem>
        <NavItem><a href="#">Guide</a></NavItem>
      </NavList>
    </Nav>
  )
}

export default Menu;