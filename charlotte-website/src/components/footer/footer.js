import React from 'react';
import styled from 'styled-components';

const Footer = styled.footer`
  box-shadow: 0 2px 20px 0 rgba(0,0,0,0.13);
  font-family: 'Heebo', sans-serif;
  flex-direction: column;
  font-size: 0.8rem;
  padding: 40px 0;
  color: #B5B5B5;
  display: flex;
  width: 100%;
`;

const SocialList = styled.ul`
  justify-content: center;
  list-style: none;
  display: flex;
  padding: 0;
`;

const SocialItem = styled.li`
  margin-left: 15px;
`;

const LegalText = styled.p`
  text-align: center;
`;

export const FooterComponent = () => {
  return(
    <Footer>
      <SocialList>
        <SocialItem>
          <a href="https://facebook.com" rel="noopener noreferrer" target="_blank">
            <img src="images/facebook.svg" alt=""/>
          </a>
        </SocialItem>
        <SocialItem>
          <a href="https://twitter.com" rel="noopener noreferrer" target="_blank">
            <img src="images/twitter.svg" alt=""/>
          </a>
        </SocialItem>
        <SocialItem>
          <a href="https://instagram.com" rel="noopener noreferrer" target="_blank">
            <img src="images/instagram.svg" alt=""/>
          </a>
        </SocialItem>
      </SocialList>
      <LegalText>© 2004-2017 Visit Charlotte. All Rights Reserved. 500 S. College Street, Suite 300, Charlotte, NC 28202</LegalText>
    </Footer>
  )
}

export default FooterComponent;