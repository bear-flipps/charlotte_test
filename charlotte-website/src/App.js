import React, { Component } from 'react';
// import styled from 'styled-components';

import HeaderComponent from './components/header/Header';
import FooterComponent from './components/footer/footer';
import DatePickerWrapper from './components/date-picker/wrapper';
import Hotels from './components/hotels/hotel';
import DateRange, { DateRage } from './model/DateRange'

class App extends Component {
  constructor() {
    super()

    this.state = {
      dateRange: DateRage(new Date(), new Date()),
    }

    this.setDateRange = this.setDateRange.bind(this)
  }

  setDateRange(dateRange) {
    this.setState({ dateRange })
  }

  render() {
    const {dateRange} = this.state

    return (
      <div className="App">
        <HeaderComponent></HeaderComponent>
        <DatePickerWrapper setDateRange={this.setDateRange}></DatePickerWrapper>
        <Hotels dateRange={dateRange}></Hotels>
        <FooterComponent></FooterComponent>
      </div>
    );
  }
}

export default App;
